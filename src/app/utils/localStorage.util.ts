export function setStorage<T>(key: string, value: T): void {
    const json = JSON.stringify(value);
    localStorage.setItem(key, json);
  }
  
  export function getStorage<T>(key: string): T | null{
    const storedValue = localStorage.getItem(key);
    if (storedValue !== null) {
      return JSON.parse(storedValue) as T;
    }
    return null;
  }
  
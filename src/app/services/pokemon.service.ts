import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'
import {Pokemon} from '../models/pokemon.model'

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  pokemonUrl: string = "https://pokeapi.co/api/v2/pokemon";
  pokemonLimit:string = '?limit=16';
  offsetString : string = '&offset=';
  
  constructor(private http: HttpClient) { }

  getPokemon(id: any): Observable<Pokemon> {
    return this.http.get<Pokemon>(`${this.pokemonUrl}/${id}`);
  }
  

  getPokemons(offset : number):Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(`${this.pokemonUrl}${this.pokemonLimit}${this.offsetString}${offset}`).pipe(
      map((resp: any) => {
        return resp.results.map((pokemon: Pokemon) => ({
          ...pokemon,
          ...this.getIdAndImage(pokemon.url)
        }))
      })
    )
  }

  private getIdAndImage(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return {id, 
      image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
  }


}

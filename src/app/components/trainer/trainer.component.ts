import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import {setStorage, getStorage} from '../../utils/localStorage.util'

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  collected = [] as string[]
  pokemons = [] as Pokemon[]
  pokemon = {} as Pokemon 

  constructor(private router: Router) { }

  ngOnInit(): void {

    let collecString = localStorage.getItem("collected");
    if(collecString != null){
      this.collected = collecString.split(',')
    }

    for(let i = 0; i < this.collected.length; i++){
      let poke = localStorage.getItem(this.collected[i])
      if(poke != null){
        this.pokemon = JSON.parse(poke)
        console.log(this.pokemon.image)
      }
      this.pokemons.push(this.pokemon)       
    }
    console.log(this.pokemons)
  }


  goToLogin(){
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }

  goToCatalogue(){
    this.router.navigateByUrl('/catalogue');

  }

  

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Pokemon} from '../../models/pokemon.model'
import { PokemonService } from '../../services/pokemon.service'

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {
  
  offset:number = 0;
  right: boolean = false;
  left : boolean = false;

  public pokemons = [] as Pokemon[]
  public pokemonsLeft = [] as Pokemon[]
  public pokemonsRight = [] as Pokemon[]

  
  constructor(private pokemonService : PokemonService, private router: Router) { }

  ngOnInit(): void {

    this.pokemonService.getPokemons(this.offset).subscribe(pokemon => {
      this.pokemons = pokemon
    })
  
    this.offset = 16;
    this.loadRight()
    console.log("init")
    
  }

  loadLeft(){
    this.pokemonService.getPokemons(this.offset).subscribe(pokemonsLeft => {
      this.pokemonsLeft = pokemonsLeft    
    })
  }

  clickLeft(){

    // Set the right pokemons to current pokemon 
    this.pokemonsRight = this.pokemons;
    // Set the current pokemons to the left
    this.pokemons = this.pokemonsLeft;
   
    // If the right button was clicked last and the value is above 32
    if(this.right && this.offset >32){
      this.offset = this.offset-48;
    }
    // If the right button was clicked and the last value was exacly 32
    else if(this.right && this.offset == 32){
      this.offset = this.offset-32;
    }
    // If the offset is >= to 16 and the right button was not last clicked
    else if(this.offset >= 16){
      this.offset = this.offset-16;
    }

    this.loadLeft();

    // Set left to true - Button left has been clicked
    this.left = true;
    // Set the right button to false
    this.right = false;

    
    
  }

  loadRight(){
    this.pokemonService.getPokemons(this.offset).subscribe(pokemonsRight => {
      this.pokemonsRight = pokemonsRight
    })
  }

  clickRight(){

    if(this.offset >= 16){
      this.pokemonsLeft = this.pokemons;
    }

    this.pokemons = this.pokemonsRight;

    if(this.left){
      this.left = false;
      this.offset = this.offset+48;
    }else{
      this.offset = this.offset+16;
    }
    
    this.right = true;
    this.loadRight();
  }

  goToDetail(id : number){
    this.router.navigateByUrl(`/details/${id}`);
  }

  goToTrainer(){
    this.router.navigateByUrl("/trainer");
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  name:string = '';

  constructor(private router: Router) { }

  ngOnInit(): void {

    if(localStorage.getItem('name') != null){
      // Go to profile page
      this.router.navigateByUrl('/catalogue');
    }
    
    
  }

  onNameChange( name : any){
    this.name = name.target.value
  }

  onLoginClick(){
    
    if(this.name.length >= 2){
      localStorage.setItem('name',this.name)
      this.router.navigateByUrl('/catalogue');
      
    }else{
      window.alert("You must enter at least two characters");
    }
  
  }



}

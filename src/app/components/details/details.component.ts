import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Router} from '@angular/router'
import {Pokemon} from '../../models/pokemon.model'
import { PokemonService } from '../../services/pokemon.service'


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  pokemon = {} as Pokemon 
  collected : boolean = false
  private id : any


  constructor(private pokemonService : PokemonService , private activatedRouter: ActivatedRoute, private router : Router) { 

    this.id = this.activatedRouter.snapshot.paramMap.get("id");
   
  }

  ngOnInit(): void {
    
    this.pokemonService.getPokemon(this.id).subscribe(pokemon => {

      this.pokemon = pokemon
      if(localStorage.getItem(this.pokemon.name) != null){
        this.collected = true;
      }
    })
  }

  collectPokemon(){

    let pokemonString = localStorage.getItem("collected")

    if(pokemonString != null){
      let pokemonArray:string[] = pokemonString.split(',');
      pokemonArray.push(this.pokemon.name);
      pokemonString = pokemonArray.join(',');
    }else{
      pokemonString = this.pokemon.name;
    }
    this.collected = true;
    localStorage.setItem("collected",pokemonString);
    localStorage.setItem(this.pokemon.name,JSON.stringify(this.pokemon))
  }

  returnToCatalogue(){
    this.router.navigateByUrl("/catalogue");
  }

  goToTrainer(){
    this.router.navigateByUrl("/trainer");
  }

}

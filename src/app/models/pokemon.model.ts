import { Stats } from './stats.model';
import { Moves } from './moves.model';
import { Types } from './types.model';
import { Abilities } from './abilities.model';
import { Sprites } from './sprites.model';


export interface Pokemon{

    id:number,
    name : string,
    image? : string, 
    types? : Types[],
    moves? :  Moves[],
    stats? : Stats[],
    height? : number,
    weight? : number,
    base_experience? :number,
    abilities? : Abilities[],
    sprites? : Sprites,
    url : string

}
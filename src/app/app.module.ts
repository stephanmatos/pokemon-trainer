import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import {HttpClientModule} from '@angular/common/http';
import { DetailsComponent } from './components/details/details.component';
import { TrainerComponent } from './components/trainer/trainer.component'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CatalogueComponent,
    DetailsComponent,
    TrainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

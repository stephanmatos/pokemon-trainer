import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGaurdGuard } from './auth/auth-gaurd.guard';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { DetailsComponent } from './components/details/details.component';
import { LoginComponent } from './components/login/login.component';
import { TrainerComponent } from './components/trainer/trainer.component';

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "catalogue",
    component: CatalogueComponent,
    canActivate : [AuthGaurdGuard]
  },
  {
    path: "details/:id",
    component: DetailsComponent,
    canActivate : [AuthGaurdGuard]
  },
  {
    path: "trainer",
    component: TrainerComponent,
    canActivate : [AuthGaurdGuard]
  },

  {
    path: "",
    pathMatch :"full",
    redirectTo : "/login"
  }

  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
